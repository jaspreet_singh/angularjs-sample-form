module.controller( "formController", function( $scope ) {
  // $scope.getName = '';
  // $scope.getEmail = '';
  // $scope.getPhnNo = ;
  $scope.hideDetails = true;
  $scope.user = {
    name: 'not entered',
    email: 'not entered',
    phnNo: 'not entered'
  };
  $scope.storeDetails = function() {
    $scope.hideDetails = false;
    $scope.user = {
      name: $scope.getName,
      email: $scope.getEmail,
      phnNo: $scope.getPhnNo
    };
  };
} );
